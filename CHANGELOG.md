# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.5.0] - 2017-04-20
### Add
- Created new consumer for event PROCESS_USER_STATE_ENTERED from transaction-service
- Created new publisher USER_PREMIUM_CONFIRMED and its errors on dead letters
- Control errors from abawebapps, wrap them into ServiceExceptions
- Added new secured endpoint /users/me

### Change
- Avoid rabbit send the errors to the dead letter, send programmatically
- Send CodeMessage code into event error headers

## [1.4.0] - 2017-02-14
- Some undocumented minor changes regarding dependency versions

## [1.3.1] - 2016-12-02
### Changed
- Updated to Aba Boot 1.4.6

## [1.3.0]
### Changed
- Updated to Aba Boot 1.4.4

## [1.2.0]
### Removed
- Remove exceptions from feign client implementation

### Changed
- Rename UserController to UsersController
- Change api/v1/user POST action
- Remove external-service-abawebapps dependencies
- Add user-service-api-model
- Mapped external classes with new api-model classes

## [1.0.1]
### Removed
- Remove spring-cloud-starter-eureka dependency in favor of kubernetes dns configuration.

## [1.0.0]
### Added
- Created new project User Service
- Created two modules User Service App and User Service Feign
- Added Feign Client and Eureka
- Added Jenkins File and Continuous Integration and Delivery
- Added JenkinsFile
- Code Coverage 100%
