# User Service

[![Build Status](http://jenkins.aba.land:8080/buildStatus/icon?job=abaenglish/backend-user-service/develop)](http://jenkins.aba.land:8080/job/abaenglish/job/backend-user-service/job/develop/)
[![Quality Gate](https://sonar.aba.land/api/badges/gate?key=com.abaenglish.backend.service%3Auser-service%3Adevelop)](https://sonar.aba.land/overview?id=com.abaenglish.backend.service%3Auser-service%3Adevelop)
[![Coverage](https://sonar.aba.land/api/badges/measure?key=com.abaenglish.backend.service%3Auser-service%3Adevelop&metric=coverage)](https://sonar.aba.land/overview?id=com.abaenglish.backend.service%3Auser-service%3Adevelop)

This microservice manages users.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [User Service](#user-service)
  - [Getting started](#getting-started)
    - [Eureka Server](#eureka-server)
  - [How to use it](#how-to-use-it)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Getting started

This section is dedicated to show how to contribute to this project setting up the local environment.

### Eureka Server

This microservice needs a eureka server you can use 

* Running a docker image: ```docker run -p 8761:8761 nexus.aba.land:5000/eureka-server```

## How to use it

This section defines how to execute user service to be used as a resource for other projects as if it was running on a server in http://localhost:8081.

You can execute it as a service using our docker image as follows: ```docker run -p 8081:8081 nexus.aba.land:5000/user-service-app:latest```
