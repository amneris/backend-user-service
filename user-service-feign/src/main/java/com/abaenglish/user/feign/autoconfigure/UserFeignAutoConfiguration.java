package com.abaenglish.user.feign.autoconfigure;

import com.abaenglish.user.feign.service.IUserServiceFeign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!test")
@EnableFeignClients(basePackages = "com.abaenglish.user.feign.service")
@ConditionalOnClass({IUserServiceFeign.class})
public class UserFeignAutoConfiguration {
}
