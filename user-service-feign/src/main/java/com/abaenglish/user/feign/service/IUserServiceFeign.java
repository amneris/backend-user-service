package com.abaenglish.user.feign.service;

import com.abaenglish.external.abawebapps.domain.InsertUserRequest;
import com.abaenglish.user.controller.v1.dto.response.CountryIsoApiResponse;
import com.abaenglish.user.controller.v1.dto.response.CurrencyApiResponse;
import com.abaenglish.user.controller.v1.dto.response.InsertUserApiResponse;
import com.abaenglish.user.controller.v1.dto.response.UserZuoraApiResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(url = "user-service", name = "user-service")
public interface IUserServiceFeign {

    @RequestMapping(value = "api/v1/users/{userId}/currency", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    CurrencyApiResponse getCurrency(@PathVariable("userId") Long userId);

    @RequestMapping(value = "api/v1/users/{userId}/country", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    CountryIsoApiResponse getCountry(@PathVariable("userId") Long userId);

    @RequestMapping(value = "api/v1/users/{userId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    UserZuoraApiResponse getUser(@PathVariable("userId") Long userId);

    @RequestMapping(value = "api/v1/users", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    InsertUserApiResponse insertUser(@RequestBody InsertUserRequest insertRequest);
}