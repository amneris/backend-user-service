package com.abaenglish.user.controller.v1.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CurrencyApiResponse {
    @JsonProperty("ABAIdCurrency")
    private String abaIdCurrency;

    public String getAbaIdCurrency() {
        return abaIdCurrency;
    }

    public void setAbaIdCurrency(String abaIdCurrency) {
        this.abaIdCurrency = abaIdCurrency;
    }

    public static final class Builder {
        private String abaIdCurrency;

        private Builder() {
        }

        public static Builder aCurrencyApiResponse() {
            return new Builder();
        }

        public Builder abaIdCurrency(String abaIdCurrency) {
            this.abaIdCurrency = abaIdCurrency;
            return this;
        }

        public CurrencyApiResponse build() {
            CurrencyApiResponse currencyApiResponse = new CurrencyApiResponse();
            currencyApiResponse.setAbaIdCurrency(abaIdCurrency);
            return currencyApiResponse;
        }
    }
}
