package com.abaenglish.user.controller.v1.dto.response;

public class ZuoraApiResponse {
    private String zuoraAccountId;
    private String status;
    private String zuoraAccountNumber;
    private String dateAdd;

    public String getZuoraAccountId() {
        return zuoraAccountId;
    }

    public void setZuoraAccountId(String zuoraAccountId) {
        this.zuoraAccountId = zuoraAccountId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getZuoraAccountNumber() {
        return zuoraAccountNumber;
    }

    public void setZuoraAccountNumber(String zuoraAccountNumber) {
        this.zuoraAccountNumber = zuoraAccountNumber;
    }

    public String getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(String dateAdd) {
        this.dateAdd = dateAdd;
    }

    public static final class Builder {
        private String zuoraAccountId;
        private String status;
        private String zuoraAccountNumber;
        private String dateAdd;

        private Builder() {
        }

        public static Builder aZuoraApiResponse() {
            return new Builder();
        }

        public Builder zuoraAccountId(String zuoraAccountId) {
            this.zuoraAccountId = zuoraAccountId;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder zuoraAccountNumber(String zuoraAccountNumber) {
            this.zuoraAccountNumber = zuoraAccountNumber;
            return this;
        }

        public Builder dateAdd(String dateAdd) {
            this.dateAdd = dateAdd;
            return this;
        }

        public ZuoraApiResponse build() {
            ZuoraApiResponse zuoraApiResponse = new ZuoraApiResponse();
            zuoraApiResponse.setZuoraAccountId(zuoraAccountId);
            zuoraApiResponse.setStatus(status);
            zuoraApiResponse.setZuoraAccountNumber(zuoraAccountNumber);
            zuoraApiResponse.setDateAdd(dateAdd);
            return zuoraApiResponse;
        }
    }
}