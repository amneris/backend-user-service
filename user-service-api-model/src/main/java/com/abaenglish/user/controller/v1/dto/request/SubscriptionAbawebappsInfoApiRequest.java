package com.abaenglish.user.controller.v1.dto.request;

public class SubscriptionAbawebappsInfoApiRequest {

    private String id;
    private String accountId;
    private String termStartDate;
    private String termEndDate;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return this.accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTermStartDate() {
        return this.termStartDate;
    }

    public void setTermStartDate(String termStartDate) {
        this.termStartDate = termStartDate;
    }

    public String getTermEndDate() {
        return this.termEndDate;
    }

    public void setTermEndDate(String termEndDate) {
        this.termEndDate = termEndDate;
    }

    public static final class Builder {
        private String id;
        private String accountId;
        private String termStartDate;
        private String termEndDate;

        private Builder() {
        }

        public static SubscriptionAbawebappsInfoApiRequest.Builder aSubscriptionAbawebappsInfo() {
            return new SubscriptionAbawebappsInfoApiRequest.Builder();
        }

        public SubscriptionAbawebappsInfoApiRequest.Builder id(String id) {
            this.id = id;
            return this;
        }

        public SubscriptionAbawebappsInfoApiRequest.Builder accountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public SubscriptionAbawebappsInfoApiRequest.Builder termStartDate(String termStartDate) {
            this.termStartDate = termStartDate;
            return this;
        }

        public SubscriptionAbawebappsInfoApiRequest.Builder termEndDate(String termEndDate) {
            this.termEndDate = termEndDate;
            return this;
        }

        public SubscriptionAbawebappsInfoApiRequest build() {
            SubscriptionAbawebappsInfoApiRequest subscriptionAbawebappsInfoApiRequest = new SubscriptionAbawebappsInfoApiRequest();
            subscriptionAbawebappsInfoApiRequest.setId(this.id);
            subscriptionAbawebappsInfoApiRequest.setAccountId(this.accountId);
            subscriptionAbawebappsInfoApiRequest.setTermStartDate(this.termStartDate);
            subscriptionAbawebappsInfoApiRequest.setTermEndDate(this.termEndDate);
            return subscriptionAbawebappsInfoApiRequest;
        }
    }
}
