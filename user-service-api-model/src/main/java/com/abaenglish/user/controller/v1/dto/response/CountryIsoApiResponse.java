package com.abaenglish.user.controller.v1.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CountryIsoApiResponse {
    @JsonProperty("iso3")
    private String abaCountryIso;
    private Long idCountry;

    public String getAbaCountryIso() {
        return abaCountryIso;
    }

    public void setAbaCountryIso(String abaCountryIso) {
        this.abaCountryIso = abaCountryIso;
    }

    public Long getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(Long idCountry) {
        this.idCountry = idCountry;
    }

    public static final class Builder {
        private String abaCountryIso;
        private Long idCountry;

        private Builder() {
        }

        public static Builder aCountryIsoApiResponse() {
            return new Builder();
        }

        public Builder abaCountryIso(String abaCountryIso) {
            this.abaCountryIso = abaCountryIso;
            return this;
        }

        public Builder idCountry(Long idCountry) {
            this.idCountry = idCountry;
            return this;
        }

        public CountryIsoApiResponse build() {
            CountryIsoApiResponse countryIsoApiResponse = new CountryIsoApiResponse();
            countryIsoApiResponse.setAbaCountryIso(abaCountryIso);
            countryIsoApiResponse.setIdCountry(idCountry);
            return countryIsoApiResponse;
        }
    }
}
