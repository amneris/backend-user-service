package com.abaenglish.user.controller.v1.dto.response;

public class UserZuoraApiResponse {
    private ZuoraApiResponse zuora;
    private UserApiResponse user;

    public ZuoraApiResponse getZuora() {
        return zuora;
    }

    public void setZuora(ZuoraApiResponse zuora) {
        this.zuora = zuora;
    }

    public UserApiResponse getUser() {
        return user;
    }

    public void setUser(UserApiResponse user) {
        this.user = user;
    }

    public static final class Builder {
        private ZuoraApiResponse zuora;
        private UserApiResponse user;

        private Builder() {
        }

        public static Builder anUserZuoraApiResponse() {
            return new Builder();
        }

        public Builder zuora(ZuoraApiResponse zuora) {
            this.zuora = zuora;
            return this;
        }

        public Builder user(UserApiResponse user) {
            this.user = user;
            return this;
        }

        public UserZuoraApiResponse build() {
            UserZuoraApiResponse userZuoraApiResponse = new UserZuoraApiResponse();
            userZuoraApiResponse.setZuora(zuora);
            userZuoraApiResponse.setUser(user);
            return userZuoraApiResponse;
        }
    }
}
