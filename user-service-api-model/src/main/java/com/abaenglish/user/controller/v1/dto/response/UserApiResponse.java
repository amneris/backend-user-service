package com.abaenglish.user.controller.v1.dto.response;

public class UserApiResponse {
    private Long id;
    private String name;
    private String surname;
    private String country;
    private String currency;
    private String email;
    private UserApiResponse.UserType userType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public enum UserType {
        FREE,
        PREMIUM;

        private UserType() {
        }
    }

    public static final class Builder {
        private Long id;
        private String name;
        private String surname;
        private String country;
        private String currency;
        private String email;
        private UserType userType;

        private Builder() {
        }

        public static Builder anUserApiResponse() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder surname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder userType(UserType userType) {
            this.userType = userType;
            return this;
        }

        public UserApiResponse build() {
            UserApiResponse userApiResponse = new UserApiResponse();
            userApiResponse.setId(id);
            userApiResponse.setName(name);
            userApiResponse.setSurname(surname);
            userApiResponse.setCountry(country);
            userApiResponse.setCurrency(currency);
            userApiResponse.setEmail(email);
            userApiResponse.setUserType(userType);
            return userApiResponse;
        }
    }
}
