package com.abaenglish.user.controller.v1;

import com.abaenglish.boot.exception.BadRequestApiException;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.user.objectmother.*;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsersControllerIT {

    @Value("${local.server.port}")
    int port;

    @MockBean
    RabbitTemplate rabbitTemplate;

    @MockBean
    private IAbawebappsExternalService abawebappsExternalService;

    private MessageConverter messageConverter = new SimpleMessageConverter();

    @Before
    public void setup() {
        Mockito.reset(abawebappsExternalService, rabbitTemplate);
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;

        //mock converter for rabbit template
        when(rabbitTemplate.getMessageConverter()).thenReturn(messageConverter);
    }

    @Test
    public void getUser() throws NotFoundExternalException {

        final Long userId = 265L;

        Mockito.when(abawebappsExternalService.existUserZuora(anyLong())).thenReturn(UserZuoraMother.getUserZuora());

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);
        builder.expectBody("user.id", is(265));
        builder.expectBody("user.name", is("Student+265"));
        builder.expectBody("user.surname", is("265"));
        builder.expectBody("user.country", is("MEX"));
        builder.expectBody("user.currency", is("MXN"));
        builder.expectBody("user.email", is("student+265@abaenglish.com"));
        builder.expectBody("zuora.zuoraAccountId", is("2c92c0f95610da1e0156127d9c115c70"));
        builder.expectBody("zuora.zuoraAccountNumber", is("A00000561"));

        ResponseSpecification responseSpec = builder.build();

        RestAssured
                .given()
                .pathParam("userId", userId)
                .when()
                .get("api/v1/users/{userId}")
                .then()
                .spec(responseSpec);
    }

    @Test
    public void getCountry() {

        final Long userId = 265L;

        Mockito.when(abawebappsExternalService.getCountry(anyLong())).thenReturn(CountryIsoMother.getCountryIsoMex());

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);
        builder.expectBody("iso3", is("MEX"));
        builder.expectBody("idCountry", is(138));

        ResponseSpecification responseSpec = builder.build();

        RestAssured
                .given()
                .pathParam("userId", userId)
                .when()
                .get("api/v1/users/{userId}/country")
                .then()
                .spec(responseSpec);
    }

    @Test
    public void getCountryError() throws NotFoundExternalException {

        final Long userId = 265L;

        doThrow(NotFoundExternalException.class).when(abawebappsExternalService).getCountry(anyLong());
        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(404);
        builder.expectBody("status", is(404));
        builder.expectBody("message", is("Country not found"));
        builder.expectBody("abaCode", is("USE0002"));

        ResponseSpecification responseSpec = builder.build();

        RestAssured
                .given()
                .pathParam("userId", userId)
                .when()
                .get("api/v1/users/{userId}/country")
                .then()
                .spec(responseSpec);
    }

    @Test
    public void getCurrencyError() throws NotFoundExternalException {

        final Long userId = 265L;

        doThrow(NotFoundExternalException.class).when(abawebappsExternalService).getCurrency(anyLong());
        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(404);
        builder.expectBody("status", is(404));
        builder.expectBody("message", is("Currency not found"));
        builder.expectBody("abaCode", is("USE0003"));

        ResponseSpecification responseSpec = builder.build();

        RestAssured
                .given()
                .pathParam("userId", userId)
                .when()
                .get("api/v1/users/{userId}/currency")
                .then()
                .spec(responseSpec);
    }

    @Test
    public void getCurrency() throws NotFoundExternalException {

        final Long userId = 265L;

        Mockito.when(abawebappsExternalService.getCurrency(anyLong())).thenReturn(CurrencyMother.getCurrency());

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);
        builder.expectBody("ABAIdCurrency", is("MXN"));

        ResponseSpecification responseSpec = builder.build();

        RestAssured
                .given()
                .pathParam("userId", userId)
                .when()
                .get("api/v1/users/{userId}/currency")
                .then()
                .spec(responseSpec);
    }

    @Test
    public void getUserError() throws NotFoundExternalException {

        final Long userId = 265L;

        doThrow(NotFoundExternalException.class).when(abawebappsExternalService).existUserZuora(anyLong());
        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(404);
        builder.expectBody("status", is(404));
        builder.expectBody("message", is("User not found"));
        builder.expectBody("abaCode", is("USE0001"));

        ResponseSpecification responseSpec = builder.build();

        RestAssured
                .given()
                .pathParam("userId", userId)
                .when()
                .get("api/v1/users/{userId}")
                .then()
                .spec(responseSpec);
    }

    @Test
    public void postUser() {

        Mockito.when(abawebappsExternalService.insertUserZuora(any())).thenReturn(InsertUserResponseMother.getinsertUserResponseSuccess());

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);
        builder.expectBody("success", is(true));

        ResponseSpecification responseSpec = builder.build();

        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(InsertUserRequestMother.getInsertUserRequestMother())
                .when()
                .post("api/v1/users")
                .then()
//                .log().all()
                .spec(responseSpec);
    }

    @Test
    public void postUserError() throws BadRequestApiException {

        final Long userId = 265L;

        doThrow(Exception.class).when(abawebappsExternalService).insertUserZuora(any());
        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(400);
        builder.expectBody("status", is(400));
        builder.expectBody("message", is("Bad request User"));
        builder.expectBody("abaCode", is("USE0004"));

        ResponseSpecification responseSpec = builder.build();

        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(InsertUserRequestMother.getInsertUserRequestMother())
                .when()
                .post("api/v1/users")
                .then()
//                .log().all()
                .spec(responseSpec);
    }
}
