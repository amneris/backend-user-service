package com.abaenglish.user.consumer;

import com.abaenglish.boot.amqp.publisher.EventPublisher;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.abawebapps.domain.UserToPremiumRequest;
import com.abaenglish.external.abawebapps.domain.UserToPremiumResponse;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.transaction.queue.PaymentTransactionEventType;
import com.abaenglish.transaction.queue.event.UserProcessStateEnteredEvent;
import com.abaenglish.user.UserServiceApplication;
import com.abaenglish.user.exception.ErrorMessages;
import com.abaenglish.user.queue.UserEventTypes;
import com.abaenglish.user.queue.consumer.ProcessUserStateEnteredConsumer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.charset.StandardCharsets;

import static com.abaenglish.user.objectmother.ProcessUserStateEnteredEventMother.getProcessUserStateEnteredEvent;
import static com.abaenglish.user.objectmother.UserToPremiumResponseMother.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {UserServiceApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class UsersConsumerIT {


    @Autowired
    private ProcessUserStateEnteredConsumer processUserStateEnteredConsumer;

    @MockBean
    RabbitTemplate rabbitTemplate;

    @MockBean
    private IAbawebappsExternalService abawebappsExternalService;

    private MessageConverter messageConverter = new SimpleMessageConverter();

    @Before
    public void setup() {
        Mockito.reset(abawebappsExternalService, rabbitTemplate);

        //mock converter for rabbit template
        when(rabbitTemplate.getMessageConverter()).thenReturn(messageConverter);
    }

    @Test
    public void processUserStateEnteredConsumer() throws ServiceException {

        final UserProcessStateEnteredEvent event = getProcessUserStateEnteredEvent();

        ArgumentCaptor<UserToPremiumRequest> argumentCaptor = ArgumentCaptor.forClass(UserToPremiumRequest.class);


        Mockito.when(abawebappsExternalService.userToPremium(any(UserToPremiumRequest.class))).thenReturn(getHttpCode200Response());

        processUserStateEnteredConsumer.onMessage(event);


        verify(abawebappsExternalService).userToPremium(argumentCaptor.capture());

        verify(rabbitTemplate).send(
                eq(UserEventTypes.USER_PREMIUM_CONFIRMED.getExchange()),
                eq(UserEventTypes.USER_PREMIUM_CONFIRMED.getRoutingKey()),
                any(Message.class));

        Assert.assertEquals(event.getUserId(), argumentCaptor.getValue().getUserId());
        Assert.assertEquals(event.getExpirationDate(), argumentCaptor.getValue().getExpirationDate());

    }

    @Test
    public void processUserStateEnteredConsumerUserAbaBadRequest() throws ServiceException {

        final UserProcessStateEnteredEvent event = getProcessUserStateEnteredEvent();
        final UserToPremiumResponse abaResponse = getHttpCode400BadRequestResponse();

        ArgumentCaptor<UserToPremiumRequest> argumentCaptor = ArgumentCaptor.forClass(UserToPremiumRequest.class);
        ArgumentCaptor<Message> eventMessageCaptor = ArgumentCaptor.forClass(Message.class);

        Mockito.when(abawebappsExternalService.userToPremium(any(UserToPremiumRequest.class))).thenReturn(abaResponse);

        processUserStateEnteredConsumer.onMessage(event);

        verify(abawebappsExternalService).userToPremium(argumentCaptor.capture());

        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.PROCESS_USER_STATE_ENTERED_DEADLETTER.getExchange()),
                eq(PaymentTransactionEventType.PROCESS_USER_STATE_ENTERED_DEADLETTER.getRoutingKey()),
                eventMessageCaptor.capture());

        Assert.assertEquals(event.getUserId(), argumentCaptor.getValue().getUserId());
        Assert.assertEquals(event.getExpirationDate(), argumentCaptor.getValue().getExpirationDate());
        checkEventErrorHeaders(ErrorMessages.ERRORS_USER_TO_PREMIUM, eventMessageCaptor, "Exception: " + abaResponse.getMessage());
        checkBodyMessageContains(eventMessageCaptor.getValue(), event.getTransactionId());
    }

    @Test
    public void processUserStateEnteredConsumerUserDoesNotExists() throws ServiceException {

        final UserProcessStateEnteredEvent event = getProcessUserStateEnteredEvent();
        final UserToPremiumResponse abaResponse = getHttpCode404UserDoesNotExistsResponse();

        ArgumentCaptor<UserToPremiumRequest> argumentCaptor = ArgumentCaptor.forClass(UserToPremiumRequest.class);
        ArgumentCaptor<Message> eventMessageCaptor = ArgumentCaptor.forClass(Message.class);

        Mockito.when(abawebappsExternalService.userToPremium(any(UserToPremiumRequest.class)))
                .thenReturn(abaResponse);

        processUserStateEnteredConsumer.onMessage(event);

        verify(abawebappsExternalService).userToPremium(argumentCaptor.capture());

        verify(rabbitTemplate).send(
                eq(PaymentTransactionEventType.PROCESS_USER_STATE_ENTERED_DEADLETTER.getExchange()),
                eq(PaymentTransactionEventType.PROCESS_USER_STATE_ENTERED_DEADLETTER.getRoutingKey()),
                eventMessageCaptor.capture());

        Assert.assertEquals(event.getUserId(), argumentCaptor.getValue().getUserId());
        Assert.assertEquals(event.getExpirationDate(), argumentCaptor.getValue().getExpirationDate());

        //validate event published
        checkEventErrorHeaders(ErrorMessages.USER_NOT_FOUND, eventMessageCaptor,"IllegalStateException: " + abaResponse.getMessage());
        checkBodyMessageContains(eventMessageCaptor.getValue(), event.getTransactionId());
    }

    private void checkEventErrorHeaders(ErrorMessages errorMessage, ArgumentCaptor<Message> eventMessageCaptor, String cause) {
        Assert.assertTrue(eventMessageCaptor.getValue().getMessageProperties().getHeaders().size() > 0);
        Assert.assertEquals(
                eventMessageCaptor.getValue().getMessageProperties().getHeaders().get(EventPublisher.MESSAGE_HEADER_EXCEPTION_CODE),
                errorMessage.getError().getCode());
        Assert.assertEquals(
                eventMessageCaptor.getValue().getMessageProperties().getHeaders().get(EventPublisher.MESSAGE_HEADER_EXCEPTION_MESSAGE),
                errorMessage.getError().getMessage());
        Assert.assertEquals(
                eventMessageCaptor.getValue().getMessageProperties().getHeaders().get(EventPublisher.MESSAGE_HEADER_EXCEPTION_CAUSE),
                cause);
    }

    private void checkBodyMessageContains(final Message message, final String expected) {
        final String body = new String(message.getBody(), StandardCharsets.UTF_8);
        Assert.assertTrue(body.contains(expected));
    }
}
