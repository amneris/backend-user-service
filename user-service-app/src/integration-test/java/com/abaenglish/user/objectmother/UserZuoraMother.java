package com.abaenglish.user.objectmother;

import com.abaenglish.external.abawebapps.domain.User;
import com.abaenglish.external.abawebapps.domain.UserZuora;
import com.abaenglish.external.abawebapps.domain.Zuora;

public class UserZuoraMother {

    public static UserZuora getUserZuora() {

        Zuora zuora = Zuora.Builder.aZuora()
                .zuoraAccountId("2c92c0f95610da1e0156127d9c115c70")
                .zuoraAccountNumber("A00000561")
                .build();
        User user = User.Builder.anUser()
                .id(265L)
                .name("Student+265")
                .surname("265")
                .email("student+265@abaenglish.com")
                .country("MEX")
                .currency("MXN")
                .build();

        UserZuora userZuora = UserZuora.Builder.anUserZuora()
                .user(user)
                .zuora(zuora)
                .build();

        return userZuora;
    }
}
