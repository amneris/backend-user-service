package com.abaenglish.user.objectmother;

import com.abaenglish.external.abawebapps.domain.InsertUserRequest;

import java.math.BigDecimal;

public class InsertUserRequestMother {

    public static InsertUserRequest getInsertUserRequestMother() {

        InsertUserRequest insertUserRequest = InsertUserRequest.Builder.anInsertUserRequest()
                .userId(9337730L)
                .zuoraAccountId("1111111111111")
                .zuoraAccountNumber("2222222222222")

                .periodId(12)
                .currency("EUR")
                .price(new BigDecimal(79.99))
                .idCountry("199")

                .idDiscount("0po9iklñ8ju5yt7gj3ndmeh3r3")
                .idRatePlanCharge("t7gj3ndmeh3r30po9iklñ8ju5y")
                .build();

        return insertUserRequest;
    }
}
