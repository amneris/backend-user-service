package com.abaenglish.user.objectmother;

import com.abaenglish.transaction.queue.event.UserProcessStateEnteredEvent;

public class ProcessUserStateEnteredEventMother {

    public static UserProcessStateEnteredEvent getProcessUserStateEnteredEvent() {

        // prepare event
        UserProcessStateEnteredEvent event = UserProcessStateEnteredEvent.Builder.anUserProcessStateEnteredEvent()
                .transactionId("1111111")
                .userId(9337827L)
                .expirationDate("2017-12-12T00:00:00")
                .build();

        return event;
    }
}
