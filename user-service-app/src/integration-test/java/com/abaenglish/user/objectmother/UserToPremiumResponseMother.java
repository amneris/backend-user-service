package com.abaenglish.user.objectmother;

import com.abaenglish.external.abawebapps.domain.UserToPremiumResponse;

public class UserToPremiumResponseMother {

    public static UserToPremiumResponse getHttpCode200Response() {

        return UserToPremiumResponse.Builder.anUserToPremiumResponse()
                .httpCode(200)
                .message("")
                .build();
    }

    public static UserToPremiumResponse getHttpCode400BadRequestResponse() {

        return UserToPremiumResponse.Builder.anUserToPremiumResponse()
                .httpCode(400)
                .message("BadRequest")
                .build();
    }

    public static UserToPremiumResponse getHttpCode404UserDoesNotExistsResponse() {

        return UserToPremiumResponse.Builder.anUserToPremiumResponse()
                .httpCode(404)
                .message("User does not exist")
                .build();
    }

}
