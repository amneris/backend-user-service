package com.abaenglish.user.objectmother;

import com.abaenglish.external.abawebapps.domain.InsertUserResponse;

public class InsertUserResponseMother {

    public static InsertUserResponse getinsertUserResponseSuccess() {

        InsertUserResponse insertUserResponse = InsertUserResponse.Builder.anInsertUserResponse()
                .success(true)
                .build();

        return insertUserResponse;
    }

    public static InsertUserResponse getinsertUserResponseError() {

        InsertUserResponse insertUserResponse = InsertUserResponse.Builder.anInsertUserResponse()
                .success(false)
                .build();

        return insertUserResponse;
    }

}
