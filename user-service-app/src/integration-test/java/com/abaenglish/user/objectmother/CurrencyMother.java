package com.abaenglish.user.objectmother;

import com.abaenglish.external.abawebapps.domain.Currency;

public class CurrencyMother {

    public static Currency getCurrency() {

        Currency currency = Currency.Builder.aCurrency()
                .abaIdCurrency("MXN")
                .build();

        return currency;
    }
}
