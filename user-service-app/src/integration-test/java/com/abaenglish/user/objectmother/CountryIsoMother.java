package com.abaenglish.user.objectmother;

import com.abaenglish.external.abawebapps.domain.CountryIso;

public class CountryIsoMother {

    public static CountryIso getCountryIsoMex() {

        CountryIso countryIso = CountryIso.Builder.aCountryIso()
                .abaCountryIso("MEX")
                .idCountry(138L)
                .build();

        return countryIso;
    }

    public static CountryIso getCountryIsoAba() {

        CountryIso countryIso = CountryIso.Builder.aCountryIso()
                .abaCountryIso("ABA")
                .idCountry(1L)
                .build();

        return countryIso;
    }
}
