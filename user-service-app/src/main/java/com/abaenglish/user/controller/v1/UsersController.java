package com.abaenglish.user.controller.v1;

import com.abaenglish.authorization.service.IAuthenticatedUserService;
import com.abaenglish.boot.exception.BadRequestApiException;
import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.external.abawebapps.domain.InsertUserRequest;
import com.abaenglish.user.controller.v1.dto.request.InsertUserApiRequest;
import com.abaenglish.user.controller.v1.dto.response.CountryIsoApiResponse;
import com.abaenglish.user.controller.v1.dto.response.CurrencyApiResponse;
import com.abaenglish.user.controller.v1.dto.response.InsertUserApiResponse;
import com.abaenglish.user.controller.v1.dto.response.UserZuoraApiResponse;
import com.abaenglish.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/v1/users", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UsersController {

    @Autowired
    private IUserService userService;

    @Autowired
    private OrikaBeanMapper mapper;

    @Autowired
    private IAuthenticatedUserService authenticatedUserService;

    @PreAuthorize("#oauth2.hasScope('read')")
    @GetMapping("/me")
    public UserZuoraApiResponse getMe() throws NotFoundApiException {
        Long userId = authenticatedUserService.getUserId();
        return mapper.map(userService.getUser(userId), UserZuoraApiResponse.class);
    }

    @RequestMapping(method = RequestMethod.POST)
    public InsertUserApiResponse insertUser(@RequestBody InsertUserApiRequest insertRequest) throws BadRequestApiException {

        return mapper.map(userService.insertUser(mapper.map(insertRequest, InsertUserRequest.class)), InsertUserApiResponse.class);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/currency")
    public CurrencyApiResponse getCurrency(@PathVariable("userId") Long userId) throws NotFoundApiException {

        return mapper.map(userService.getCurrency(userId), CurrencyApiResponse.class);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/country")
    public CountryIsoApiResponse getCountry(@PathVariable("userId") Long userId) throws NotFoundApiException {

        return mapper.map(userService.getCountry(userId), CountryIsoApiResponse.class);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}")
    public UserZuoraApiResponse getUser(@PathVariable("userId") Long userId) throws NotFoundApiException {

        return mapper.map(userService.getUser(userId), UserZuoraApiResponse.class);
    }

}
