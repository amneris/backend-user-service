package com.abaenglish.user.exception;

import com.abaenglish.boot.exception.service.ServiceException;

public class NotFoundServiceException extends ServiceException {
    public NotFoundServiceException(ErrorMessages error, Throwable cause) { super(error.getError(), cause); }
}
