package com.abaenglish.user.exception;

import com.abaenglish.boot.exception.CodeMessage;

public enum ErrorMessages {

    USER_NOT_FOUND(new CodeMessage("USE0001", "User not found")),
    COUNTRY_NOT_FOUND(new CodeMessage("USE0002", "Country not found")),
    CURRENCY_NOT_FOUND(new CodeMessage("USE0003", "Currency not found")),
    PROBLEM_INSERT_USER(new CodeMessage("USE0004", "Bad request User")),
    ERRORS_USER_TO_PREMIUM(new CodeMessage("USE0005", "Cannot convert user to premium in aba")),
    PROBLEM_USER_TO_PREMIUM(new CodeMessage("USE0006", "There was error trying to convert user to premium"));

    private final CodeMessage error;

    ErrorMessages(CodeMessage errorMessage) {
        error = new CodeMessage(errorMessage.getCode(), errorMessage.getMessage());
    }

    public CodeMessage getError() {
        return error;
    }

}
