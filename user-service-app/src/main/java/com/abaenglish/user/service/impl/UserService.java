package com.abaenglish.user.service.impl;

import com.abaenglish.boot.exception.BadRequestApiException;
import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.external.abawebapps.domain.*;
import com.abaenglish.external.abawebapps.exception.NotFoundExternalException;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.user.exception.ErrorMessages;
import com.abaenglish.user.exception.NotFoundServiceException;
import com.abaenglish.user.service.IUserService;
import com.abaenglish.user.service.dto.user.UserToPremiumServiceRequest;
import com.abaenglish.user.service.dto.user.UserToPremiumServiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements IUserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private IAbawebappsExternalService abawebappsExternalService;

    @Autowired
    private OrikaBeanMapper mapper;

    @Deprecated
    @Override
    public InsertUserResponse insertUser(InsertUserRequest insertUserRequest) throws BadRequestApiException {

        InsertUserResponse user;

        try {
            user = abawebappsExternalService.insertUserZuora(insertUserRequest);
        } catch (Exception e) {
            logger.error("Problem insert User: " + e);
            throw new BadRequestApiException(ErrorMessages.PROBLEM_INSERT_USER.getError());
        }
        return user;
    }

    @Override
    public Currency getCurrency(Long userId) throws NotFoundApiException {

        Currency currency;

        try {
            currency = abawebappsExternalService.getCurrency(userId);
        } catch (Exception e) {
            logger.error("Problem obtain Currency: " + e);
            throw new NotFoundApiException(ErrorMessages.CURRENCY_NOT_FOUND.getError());
        }
        return currency;
    }

    @Override
    public CountryIso getCountry(Long userId) throws NotFoundApiException {

        CountryIso countryIso;

        try {
            countryIso = abawebappsExternalService.getCountry(userId);
        } catch (Exception e) {
            logger.error("Problem obtain Country: " + e);
            throw new NotFoundApiException(ErrorMessages.COUNTRY_NOT_FOUND.getError());
        }

        return countryIso;
    }

    @Override
    public UserZuora getUser(Long userId) throws NotFoundApiException {
        UserZuora userZuora;
        try {
            userZuora = abawebappsExternalService.existUserZuora(userId);
        } catch (NotFoundExternalException e) {
            logger.error("Problem obtain user: " + e);
            throw new NotFoundApiException(ErrorMessages.USER_NOT_FOUND.getError());
        }

        return userZuora;
    }

    @Override
    public UserToPremiumServiceResponse userToPremium(UserToPremiumServiceRequest userToPremiumServiceRequest) throws ServiceException {

        try {
            UserToPremiumResponse response = abawebappsExternalService.userToPremium(mapper.map(userToPremiumServiceRequest, UserToPremiumRequest.class));
            final Optional<ServiceException> errors = parseAbaResponse(response.getHttpCode(), response.getMessage());
            if(errors.isPresent()) {
                throw errors.get();
            }
            return mapper.map(response, UserToPremiumServiceResponse.class);

        } catch (Exception e) {
            if(e instanceof ServiceException) {
                throw e;
            } else {
                logger.error("Problems converting user to premium: {}", e);
                throw new ServiceException(ErrorMessages.PROBLEM_USER_TO_PREMIUM.getError(), e);
            }
        }
    }

    private Optional<ServiceException> parseAbaResponse(final int code, final String message) {
        switch (code) {
            case 200:
                return Optional.empty();
            case 404:
                return Optional.of(new NotFoundServiceException(ErrorMessages.USER_NOT_FOUND,
                                new IllegalStateException(message)));
            default:
                return Optional.of(new ServiceException(
                        ErrorMessages.ERRORS_USER_TO_PREMIUM.getError(),
                        new Exception(message)));
        }
    }

}
