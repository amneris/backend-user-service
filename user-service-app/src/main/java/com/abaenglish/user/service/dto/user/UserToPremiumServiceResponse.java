package com.abaenglish.user.service.dto.user;

public class UserToPremiumServiceResponse {
    private String transactionId;
    private Integer httpCode;
    private String message;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static final class Builder {
        private String transactionId;
        private Integer httpCode;
        private String message;

        private Builder() {
        }

        public static Builder anUserToPremiumRes() {
            return new Builder();
        }

        public Builder witTransactionId(String transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public Builder witHttpCode(Integer httpCode) {
            this.httpCode = httpCode;
            return this;
        }

        public Builder witMessage(String message) {
            this.message = message;
            return this;
        }

        public UserToPremiumServiceResponse build() {
            UserToPremiumServiceResponse userToPremiumServiceResponse = new UserToPremiumServiceResponse();
            userToPremiumServiceResponse.setTransactionId(transactionId);
            userToPremiumServiceResponse.setHttpCode(httpCode);
            userToPremiumServiceResponse.setMessage(message);
            return userToPremiumServiceResponse;
        }
    }
}
