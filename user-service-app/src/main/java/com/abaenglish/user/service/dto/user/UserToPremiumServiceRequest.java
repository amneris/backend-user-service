package com.abaenglish.user.service.dto.user;

public class UserToPremiumServiceRequest {

    private String transactionId;
    private Long userId;
    private String expirationDate;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public static final class Builder {
        private String transactionId;
        private Long userId;
        private String expirationDate;

        private Builder() {
        }

        public static Builder anUserToPremiumRequest() {
            return new Builder();
        }

        public Builder transactionId(String transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public UserToPremiumServiceRequest build() {
            UserToPremiumServiceRequest userToPremiumServiceRequest = new UserToPremiumServiceRequest();
            userToPremiumServiceRequest.setTransactionId(transactionId);
            userToPremiumServiceRequest.setUserId(userId);
            userToPremiumServiceRequest.setExpirationDate(expirationDate);
            return userToPremiumServiceRequest;
        }
    }
}
