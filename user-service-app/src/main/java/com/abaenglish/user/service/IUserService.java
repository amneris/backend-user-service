package com.abaenglish.user.service;

import com.abaenglish.boot.exception.BadRequestApiException;
import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.external.abawebapps.domain.*;
import com.abaenglish.user.service.dto.user.UserToPremiumServiceRequest;
import com.abaenglish.user.service.dto.user.UserToPremiumServiceResponse;

public interface IUserService {

    InsertUserResponse insertUser(InsertUserRequest insertUserRequest) throws BadRequestApiException;

    Currency getCurrency(Long userId) throws NotFoundApiException;

    CountryIso getCountry(Long userId) throws NotFoundApiException;

    UserZuora getUser(Long userId) throws NotFoundApiException;

    UserToPremiumServiceResponse userToPremium(UserToPremiumServiceRequest userToPremiumServiceRequest) throws ServiceException;
}
