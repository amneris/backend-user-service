package com.abaenglish.user.queue.event;

import com.abaenglish.boot.amqp.domain.DomainEvent;
import com.abaenglish.boot.amqp.domain.DomainEventType;
import com.abaenglish.user.queue.UserEventTypes;

public class UserPremiumConfirmedEvent extends DomainEvent {

    private static final long serialVersionUID = -3380876998077330620L;

    private String transactionId;

    public UserPremiumConfirmedEvent() {
        super(UserEventTypes.USER_PREMIUM_CONFIRMED);
    }

    protected UserPremiumConfirmedEvent(DomainEventType type) { super(type); }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public static final class Builder {
        private String transactionId;
        private DomainEventType type;

        private Builder() {
        }

        public static Builder anUserPremiumConfirmedEvent() {
            return new Builder();
        }

        public static Builder anUserPremiumConfirmedErrorEvent() {
            return new Builder().type(UserEventTypes.USER_PREMIUM_CONFIRMED_DEADLETTERS);
        }


        public Builder transactionId(String transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public Builder type(DomainEventType type) {
            this.type = type;
            return this;
        }

        public UserPremiumConfirmedEvent build() {
            UserPremiumConfirmedEvent userPremiumConfirmedEvent = type != null ? new UserPremiumConfirmedEvent(type) : new UserPremiumConfirmedEvent();
            userPremiumConfirmedEvent.setTransactionId(transactionId);
            return userPremiumConfirmedEvent;
        }
    }
}
