package com.abaenglish.user.queue.consumer;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.transaction.queue.event.UserProcessStateEnteredEvent;
import com.abaenglish.user.queue.publisher.UserEventPublisher;
import com.abaenglish.user.service.IUserService;
import com.abaenglish.user.service.dto.user.UserToPremiumServiceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProcessUserStateEnteredConsumer {

    public static final String QUEUE_NAME = "user-service.process-user-state-entered";

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessUserStateEnteredConsumer.class);

    @Autowired
    private IUserService userService;

    @Autowired
    private UserEventPublisher userEventPublisher;

    @Autowired
    private OrikaBeanMapper mapper;

    @RabbitListener(queues = QUEUE_NAME, containerFactory = "rabbitRetryListenerContainerFactory")
    public void onMessage(final UserProcessStateEnteredEvent event) throws ServiceException {
        LOGGER.info("Received from {} queue <{}> with user id {}", QUEUE_NAME, event.getRoutingKey(), event.getUserId());
        try {
            userService.userToPremium(mapper.map(event, UserToPremiumServiceRequest.class));
        } catch (Exception e) {
            // process errors and send to deadletter
            final UserProcessStateEnteredEvent errorEvent = UserProcessStateEnteredEvent.Builder
                    .anUserProcessStateEnteredDeadletterEvent()
                    .transactionId(event.getTransactionId())
                    .build();
            LOGGER.error(String.format("There was an error trying to convert user to premium send event to the deadletter [%s]", errorEvent.toString()), e);
            userEventPublisher.sendErrorToDeadletter(errorEvent, e);
        }
    }
}
