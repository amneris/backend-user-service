package com.abaenglish.user.queue.publisher;

import com.abaenglish.boot.amqp.domain.DomainEvent;
import com.abaenglish.boot.amqp.publisher.EventPublisher;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.user.queue.event.UserPremiumConfirmedEvent;
import com.abaenglish.user.service.dto.user.UserToPremiumServiceRequest;
import com.abaenglish.user.service.dto.user.UserToPremiumServiceResponse;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Aspect
@Component
public class UserEventPublisher extends EventPublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserEventPublisher.class);

    private void sendEventToExchange(DomainEvent event) {
        LOGGER.info("Sending event for exchange [{}] and queue [{}], type [{}]", event.getExchange(), event.getRoutingKey(), event.getEventType());
        sendEvent(event);
    }

    public void sendErrorToDeadletter(DomainEvent event, Throwable e) {
        Optional<String> codeMessage = Optional.empty();
        if(e instanceof ServiceException && ((ServiceException) e).getCodeMessage() != null) {
            codeMessage = Optional.of(((ServiceException) e).getCodeMessage().getCode());
        }
        LOGGER.info("Sending ERROR event for exchange [{}] and queue [{}], type [{}] and code message {}", event.getExchange(), event.getRoutingKey(), event.getEventType(), codeMessage.get());
        sendError(event, codeMessage, Optional.of(e));
    }

    @AfterReturning(pointcut = "execution(* com.abaenglish.user.service.IUserService.userToPremium(..))", returning = "userToPremiumServiceResponse")
    public void processUserToPremium(JoinPoint joinPoint, UserToPremiumServiceResponse userToPremiumServiceResponse) {
        final UserToPremiumServiceRequest eventRequest = (UserToPremiumServiceRequest)joinPoint.getArgs()[0];
        final UserPremiumConfirmedEvent userPremiumConfirmedEvent = UserPremiumConfirmedEvent.Builder.anUserPremiumConfirmedEvent()
                .transactionId(eventRequest.getTransactionId())
                .build();
        sendEventToExchange(userPremiumConfirmedEvent);
    }
}
