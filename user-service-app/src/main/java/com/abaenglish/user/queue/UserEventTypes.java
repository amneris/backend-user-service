package com.abaenglish.user.queue;

import com.abaenglish.boot.amqp.domain.DomainEventType;

public enum UserEventTypes implements DomainEventType {

    USER_PREMIUM_CONFIRMED("user-service", "user-service.user-premium-confirmed"),
    USER_PREMIUM_CONFIRMED_DEADLETTERS("user-service", "user-service.user-premium-confirmed.deadletters");

    private String exchange;
    private String routingKey;

    UserEventTypes(String exchange, String routingKey) {
        this.exchange = exchange;
        this.routingKey = routingKey;
    }

    @Override
    public String getExchange() {
        return exchange;
    }

    @Override
    public String getRoutingKey() {
        return routingKey;
    }
}
