package com.abaenglish.transaction.queue;

import com.abaenglish.boot.amqp.domain.DomainEventType;

public enum PaymentTransactionEventType implements DomainEventType {

    PROCESS_USER_STATE_ENTERED("transaction-service", "user-service.process-user-state-entered"),
    PROCESS_USER_STATE_ENTERED_DEADLETTER("transaction-service", "user-service.process-user-state-entered.deadletters");

    private String exchange;
    private String routingKey;

    PaymentTransactionEventType(String exchange, String routingKey) {
        this.exchange = exchange;
        this.routingKey = routingKey;
    }

    @Override
    public String getExchange() {
        return exchange;
    }

    @Override
    public String getRoutingKey() {
        return routingKey;
    }
}