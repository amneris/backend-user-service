package com.abaenglish.transaction.queue.event;

import com.abaenglish.boot.amqp.domain.DomainEvent;
import com.abaenglish.boot.amqp.domain.DomainEventType;
import com.abaenglish.transaction.queue.PaymentTransactionEventType;

public class UserProcessStateEnteredEvent extends DomainEvent {

    private static final long serialVersionUID = 5249266297610611034L;
    private String transactionId;
    private Long userId;
    private String provider;
    private String expirationDate;

    public UserProcessStateEnteredEvent() {super(PaymentTransactionEventType.PROCESS_USER_STATE_ENTERED); }

    protected UserProcessStateEnteredEvent(DomainEventType type) {super(type); }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public static final class Builder {
        private String transactionId;
        private Long userId;
        private String provider;
        private String expirationDate;
        private DomainEventType type;

        private Builder() {
        }

        public static Builder anUserProcessStateEnteredEvent() {
            return new Builder();
        }

        public static Builder anUserProcessStateEnteredDeadletterEvent() {
            return new Builder().type(PaymentTransactionEventType.PROCESS_USER_STATE_ENTERED_DEADLETTER);
        }

        public Builder transactionId(String transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public Builder type(DomainEventType type) {
            this.type = type;
            return this;
        }


        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder provider(String provider) {
            this.provider = provider;
            return this;
        }

        public Builder expirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public UserProcessStateEnteredEvent build() {
            UserProcessStateEnteredEvent userProcessStateEnteredEvent = new UserProcessStateEnteredEvent();
            if(type != null) {
                userProcessStateEnteredEvent = new UserProcessStateEnteredEvent(type);
            }
            userProcessStateEnteredEvent.setTransactionId(transactionId);
            userProcessStateEnteredEvent.setUserId(userId);
            userProcessStateEnteredEvent.setProvider(provider);
            userProcessStateEnteredEvent.setExpirationDate(expirationDate);
            return userProcessStateEnteredEvent;
        }
    }
}